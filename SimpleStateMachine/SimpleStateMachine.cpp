// SimpleStateMachine.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "windows.h"
#include <iostream>

using namespace std;

char m_state = NULL;

enum state_id_t
{
	sid_StateA = 'A',
	sid_StateB,
	sid_StateC,
	sid_StateD,
};

int main()
{
	cout << "===== Start =====" << endl;

	m_state = sid_StateA;
	while (true)
	{
		char d;

		cout << "Current State : " << m_state << endl;
		cout << "Please input D value(1,2,5,6,7) : ";
		cin >> d;

		switch (d)
		{
		case '1':
			if (m_state == sid_StateA)
				m_state = sid_StateB;
			break;
		case '2':
			if (m_state == sid_StateB)
				m_state = sid_StateC;
			break;
		case '5':
			if (m_state == sid_StateA)
				m_state = sid_StateD;
			break;
		case '6':
			if (m_state == sid_StateB)
				m_state = sid_StateD;
			break;
		case '7':
			if (m_state == sid_StateC)
				m_state = sid_StateD;
			break;
		case 'q':
			cout << "====== End ======" << endl;
			Sleep(5000);
			return 0;
		default:
			cout << "Invalid input..." << endl;
		}
	}

	return 0;
}
